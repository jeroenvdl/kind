#!/bin/sh

# in a pod the configuration file is implemented as a symbolic link
# when the configmap is updated the update of the symbolic link causes a moved_to event
monitorConfig() {
  while true; do
    inotifywait -e moved_to -m $(dirname ${PROXY_CONFIG}) | while read -r directory events filename; do
      echo "$(date +%FT%TZ) Proxy configuration ${PROXY_CONFIG} was updated; reload HAProxy..."
      kill -USR2 1
    done
  done
}

# if the PROXY_CONFIG variable isn't set, use the default implementation of the haproxy container
if [ -z "${PROXY_CONFIG}" ]; then
  # first arg is `-f` or `--some-option`
  if [ "${1#-}" != "$1" ]; then
    set -- haproxy "$@"
  fi

  if [ "$1" = 'haproxy' ]; then
    shift # "haproxy"
    # if the user wants "haproxy", let's add a couple useful flags
    #   -W  -- "master-worker mode" (similar to the old "haproxy-systemd-wrapper"; allows for reload via "SIGUSR2")
    #   -db -- disables background mode
    set -- haproxy -W -db "$@"
  fi
else
  monitorConfig &
  set -- haproxy -W -db -f ${PROXY_CONFIG} "$@"
fi

echo "$(date +%FT%TZ) Start HAProxy with arguments: $@"
exec "$@"