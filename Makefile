.PHONY: all dashboard argo-cd argo-workflows argo-events tekton amq-operator amq-broker amq-start amq-delete amq-publish amq-consume keycloak keycloak-start skupper skupper-deploy-sites skupper-link-sites cert-manager

info:
	@echo "Available commands:"
	@echo ""
	@echo "Command                              Description"
	@echo "-------                              -----------"
	@echo "make info                            Show the available make commands"
	@echo "make start                           Start the Kind environment"
	@echo "make delete                          Delete the Kind environment"
	@echo "make dashboard                       Setup Kubernetes Dashboard"
	@echo "make metrics                         Setup metrics-server"
	@echo "make ingress                         Setup Ingress NGINX"
	@echo "make argo-cd                         Setup Argo CD"
	@echo "make argo-workflows                  Setup Argo Workflows"
	@echo "make argo-events                     Setup Argo Events"
	@echo "make tekton                          Setup Tekton Pipelines"
	@echo "make registry                        Setup registry"
	@echo "make amq-operator                    Setup AMQ operator and broker"
	@echo "make amq-start                       Apply AMQ broker configuraion and addresses"
	@echo "make amq-delete                      Delete AMQ broker and addresses"
	@echo "make amq-publish                     Publish messages"
	@echo "make amq-consume                     Consume messages"
	@echo "make keycloak                        Setup Keycloak"
	@echo "make keycloak-start                  Start a Keycloak instance"
	@echo "make skupper                         Setup site controller"
	@echo "make skupper-deploy-sites            Setup sites east and west"
	@echo "make skupper-link-sites              Link sites east and west"
	@echo "make cert-manager                    Setup cert-manager"
	@echo ""

start:
	@echo "=== Starting the Kind environment ==="
	kind create cluster --config kind-config.yaml

delete:
	@echo "=== Deleting the Kind environment ==="
	kind delete cluster

dashboard:
	@echo "=== Deploy Kubernetes Dashboard environment ==="
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
	kubectl apply -f dashboard/service-account.yaml
	kubectl apply -f dashboard/secret.yaml	
	kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
	echo "Dashboard proxy URL: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/"

ingress:
	@echo "=== Deploy NGINX Ingress environment ==="
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml

metrics:
	@echo "=== Set Metrics environment ==="
	kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.7.0/components.yaml
	kubectl patch deployment metrics-server -n kube-system -p '{"spec":{"template":{"spec":{"containers":[{"name":"metrics-server","args":["--cert-dir=/tmp", "--secure-port=10250", "--kubelet-insecure-tls","--kubelet-preferred-address-types=InternalIP"]}]}}}}'

argo-cd:
	@echo "=== Deploy ArgodCD environment ==="
	kubectl create namespace argocd
	kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
	kubectl apply -n argocd -f argo/cd/ingress.yaml
	@echo "Argo admin password: "
	kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2

argo-workflows:
	@echo "=== Deploy ArgodCD environment ==="
	kubectl create namespace argo | true
	kubectl config set-context --current --namespace argo
	kubectl apply -n argo -f https://github.com/argoproj/argo-workflows/releases/download/v3.5.10/install.yaml
	kubectl create clusterrole argowork --verb="*" --resource=workflows.argoproj.io,cronworkflows.argoproj.io,workflowartifactgctasks.argoproj.io,workfloweventbindings.argoproj.io,workflowtaskresults.argoproj.io,workflowtasksets.argoproj.io,workflowtemplates.argoproj.io,clusterworkflowtemplates.argoproj.io,eventsources.argoproj.io,sensors.argoproj.io
	kubectl create sa argowork -n argo
	kubectl create clusterrolebinding argowork --clusterrole=argowork --serviceaccount=argo:argowork
	kubectl apply -f argo/workflows/token.yaml

argo-events:
	kubectl create namespace argo-events | true
	kubectl config set-context --current --namespace argo-events
	kubectl apply -f https://raw.githubusercontent.com/argoproj/argo-events/stable/manifests/install.yaml
	kubectl apply -f https://raw.githubusercontent.com/argoproj/argo-events/stable/manifests/install-validating-webhook.yaml
	kubectl apply -n argo-events -f https://raw.githubusercontent.com/argoproj/argo-events/stable/examples/eventbus/native.yaml
	kubectl apply -n argo-events -f https://raw.githubusercontent.com/argoproj/argo-events/stable/examples/event-sources/webhook.yaml

tekton:
	@echo "=== Deploy Tekton Pipelines environment ==="
	kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml
	kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml
	kubectl apply --filename https://storage.googleapis.com/tekton-releases/dashboard/latest/tekton-dashboard-release.yaml
	kubectl -n tekton-pipelines apply -f tekton/

registry:
	@echo "=== Deploy Registry ==="
	# create registry container unless it already exists
	reg_name='kind-registry'
	reg_port='5000'
	running="docker inspect -f '{{.State.Running}}' "kind-registry" 2>/dev/null || true)"
	if [ "$$running" != 'true' ]; then \
		docker run -d --restart=always -p "5000:5000" --name "kind-registry" registry:2; \
	fi
	
	docker network connect "kind" "kind-registry"
	
	for node in `kind get nodes`; do \
		kubectl annotate node $$node "kind.x-k8s.io/registry=localhost:5000"; \
	done

amq-operator:
	kubectl create namespace activemq-artemis-operator | true
	git clone https://github.com/artemiscloud/activemq-artemis-operator.git | true
	kubectl config set-context --current --namespace activemq-artemis-operator
	./activemq-artemis-operator/deploy/cluster_wide_install_opr.sh

amq-broker:	
	#keytool -genkeypair -alias artemis -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore amq/tls/broker.ks -validity 3000
	#keytool -export -alias artemis -file amq/tls/broker.cert -keystore amq/tls/broker.ks
	#keytool -import -v -trustcacerts -alias artemis -file amq/tls/broker.cert -keystore amq/tls/client.ts
	
	#printf "password\npassword\n${CLUSTER_IP}.nip.io\nArtemisCloud\nConclusion XForce\nCuijk\nNoord-Brabant\nNL\nyes\n" | keytool -genkeypair -alias artemis -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore amq/tls/broker.ks -validity 3000
    #printf 'password\n' | keytool -export -alias artemis -file amq/tls/broker.cert -keystore amq/tls/broker.ks
    #printf 'password\password\nyes\n' | keytool -import -v -trustcacerts -alias artemis -file amq/tls/broker.cert -keystore amq/tls/client.ts
	
	kubectl create secret generic amqps-tls-secret --from-file=amq/tls/broker.ks --from-file=amq/tls/client.ts --from-literal=keyStorePassword='password' --from-literal=trustStorePassword='password'
	kubectl apply -f amq/artemis.yaml
	kubectl apply -f amq/address_queue.yaml
	kubectl apply -f amq/ingress.yaml

amq-delete:
	kubectl delete -f amq/artemis.yaml
	kubectl delete -f amq/address_queue.yaml
	kubectl delete secret secret-address-artemis-address-queue

amq-start:
	kubectl apply -f amq/artemis.yaml
	kubectl apply -f amq/address_queue.yaml

amq-publish:
	kubectl exec artemis-broker-ss-0 -- amq-broker/bin/artemis producer --url tcp://artemis-broker-ss-0:61616 --destination=topic://myAddress0 --message-count=100
	kubectl exec artemis-broker-ss-0 --container artemis-broker-container -- amq-broker/bin/artemis queue stat

amq-consume:
	kubectl exec artemis-broker-ss-0 -- amq-broker/bin/artemis consumer --url tcp://artemis-broker-ss-0:61616 --destination=topic://myAddress0::myQueue0 --break-on-null
	kubectl exec artemis-broker-ss-0 -- amq-broker/bin/artemis consumer --url tcp://artemis-broker-ss-0:61616 --destination=topic://myAddress0::myQueue1 --break-on-null
	kubectl exec artemis-broker-ss-0 -- amq-broker/bin/artemis consumer --url tcp://artemis-broker-ss-0:61616 --destination=topic://myAddress0::myQueue2 --break-on-null
	kubectl exec artemis-broker-ss-0 --container artemis-broker-container -- amq-broker/bin/artemis queue stat

keycloak:
	kubectl create namespace keycloak-operator
	kubectl config set-context --current --namespace keycloak-operator
	kubectl apply -f https://raw.githubusercontent.com/keycloak/keycloak-k8s-resources/25.0.1/kubernetes/keycloaks.k8s.keycloak.org-v1.yml
	kubectl apply -f https://raw.githubusercontent.com/keycloak/keycloak-k8s-resources/25.0.1/kubernetes/keycloakrealmimports.k8s.keycloak.org-v1.yml
	kubectl apply -f https://raw.githubusercontent.com/keycloak/keycloak-k8s-resources/25.0.1/kubernetes/kubernetes.yml

keycloak-start:
	openssl req -subj '/CN=keycloak.dev.lan/O=Test Keycloak./C=NL' -newkey rsa:2048 -nodes -keyout keycloak/key.pem -x509 -days 365 -out keycloak/certificate.pem
	kubectl -n keycloak-operator apply -f keycloak/postgres.yaml
	kubectl -n keycloak-operator create secret tls keycloak-tls-secret --cert keycloak/certificate.pem --key keycloak/key.pem
	kubectl -n keycloak-operator create secret generic keycloak-db-secret --from-literal=username=testuser --from-literal=password=testpassword
	kubectl -n keycloak-operator apply -f keycloak/kc.yaml
	kubectl -n keycloak-operator get secret kc-initial-admin -o jsonpath='{.data.username}' | base64 --decode
	kubectl -n keycloak-operator get secret kc-initial-admin -o jsonpath='{.data.password}' | base64 --decode

skupper:
	kubectl create namespace skupper-site-controller
	kubectl config set-context --current --namespace skupper-site-controller
	kubectl apply -f skupper/deploy-watch-all-ns.yaml

skupper-deploy-sites:
	kubectl create namespace skupper-site-east
	kubectl create namespace skupper-site-west
	kubectl apply -f skupper/site-east.yaml
	kubectl apply -f skupper/site-west.yaml

skupper-link-sites:
	kubectl -n skupper-site-east apply -f skupper/site-east-token-request.yaml
	sleep 30
	kubectl -n skupper-site-east get secret -o yaml link-site-east | yq 'del(.metadata.namespace)' > skupper/site-east-token.yaml
	kubectl -n skupper-site-west apply -f skupper/site-east-token.yaml
	# = annotate a service = 
	# kubectl annotate service artemis-broker-amqp-0-svc "skupper.io/address=van-east-artemis" "skupper.io/proxy=tcp" "skupper.io/target=artemis-broker-amqp-0-svc"

cert-manager:
	kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.15.3/cert-manager.yaml	
