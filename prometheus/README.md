https://medium.com/@seifeddinerajhi/monitoring-kubernetes-clusters-with-kube-state-metrics-2b9e73a67895

```
helm install --wait --timeout 15m    --namespace monitoring --create-namespace    --repo https://prometheus-community.github.io/helm-charts    kube-prometheus-stack kube-prometheus-stack --values - <<EOF 
 kubeEtcd:
   service:
     targetPort: 2381
 kubeControllerManager:  
   service:
     targetPort: 10257
 kubeScheduler:
   service:
     targetPort: 10259
EOF
```

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts && helm repo update && helm install kube-state-metrics prometheus-community/kube-state-metrics -n kube-system
```
